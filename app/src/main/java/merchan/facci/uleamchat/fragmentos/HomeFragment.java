package merchan.facci.uleamchat.fragmentos;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import merchan.facci.uleamchat.Adaptadores.Adapter;
import merchan.facci.uleamchat.Adaptadores.AdapterAdmin;
import merchan.facci.uleamchat.AddHotel;
import merchan.facci.uleamchat.MainActivity;
import merchan.facci.uleamchat.Modelo.DatosHerra;
import merchan.facci.uleamchat.PrincipalAdminActivity;
import merchan.facci.uleamchat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    RecyclerView recyclerViewCatalogo;
    DatabaseReference mRefCatalogo;
    ArrayList<DatosHerra> listCatalogo;
    AdapterAdmin adapter;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    ActionBar actionBar;

    public HomeFragment() {

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);



        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            firebaseAuth.signOut();

        }
        if (id == R.id.action_add_herra) {

        }

        return super.onOptionsItemSelected(item);
    }
}

