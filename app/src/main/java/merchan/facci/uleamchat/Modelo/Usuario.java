package merchan.facci.uleamchat.Modelo;

public class Usuario {

    String Correo, Nombre, Direcion, apellido, uid, Foto, Telefono, Cargo;

    public Usuario() {
    }

    public Usuario(String correo, String nombre, String direcion, String apellido, String uid, String foto, String telefono, String cargo) {
        Correo = correo;
        Nombre = nombre;
        Direcion = direcion;
        this.apellido = apellido;
        this.uid = uid;
        Foto = foto;
        Telefono = telefono;
        Cargo = cargo;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDirecion() {
        return Direcion;
    }

    public void setDirecion(String direcion) {
        Direcion = direcion;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String cargo) {
        Cargo = cargo;
    }
}
