package merchan.facci.uleamchat.Modelo;

public class Herramientas {
    private String Nombre, Precio, Descripcion, urlFoto, Codigo, Fecha;

    public Herramientas() {
    }

    public Herramientas(String nombre, String precio, String descripcion, String urlFoto, String codigo, String fecha) {
        Nombre = nombre;
        Precio = precio;
        Descripcion = descripcion;
        this.urlFoto = urlFoto;
        Codigo = codigo;
        Fecha = fecha;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String precio) {
        Precio = precio;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }
}

