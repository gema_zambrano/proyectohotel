package merchan.facci.uleamchat.Modelo;

public class User {
    String Nombre, Direcion, nombrehotel, uid, contrasena, Telefono, correo;

    public User() {
    }

    public User(String nombre, String direcion, String nombrehotel, String uid, String contrasena, String telefono, String correo) {
        Nombre = nombre;
        Direcion = direcion;
        this.nombrehotel = nombrehotel;
        this.uid = uid;
        this.contrasena = contrasena;
        Telefono = telefono;
        this.correo = correo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDirecion() {
        return Direcion;
    }

    public void setDirecion(String direcion) {
        Direcion = direcion;
    }

    public String getNombrehotel() {
        return nombrehotel;
    }

    public void setNombrehotel(String nombrehotel) {
        this.nombrehotel = nombrehotel;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}