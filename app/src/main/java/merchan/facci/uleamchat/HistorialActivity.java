package merchan.facci.uleamchat;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import merchan.facci.uleamchat.Adaptadores.AdapterPedido;
import merchan.facci.uleamchat.Modelo.DatosHerra;


public class HistorialActivity extends AppCompatActivity {

    RecyclerView recyclerViewCatalogo;
    DatabaseReference mRefCatalogo;
    FloatingActionButton AggCarrito;
    ArrayList<DatosHerra> listCatalogo;
    AdapterPedido adapter;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        firebaseAuth = FirebaseAuth.getInstance();

        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        actionBar = getSupportActionBar();
        actionBar.setTitle("Listado de Hoteles");
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);


        recyclerViewCatalogo = findViewById(R.id.recyclerview_catalogo1);
        recyclerViewCatalogo.setLayoutManager(new LinearLayoutManager(this));
        listCatalogo = new ArrayList<DatosHerra>();

        mRefCatalogo = FirebaseDatabase.getInstance().getReference().child("Hoteles");



        mRefCatalogo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    DatosHerra c = dataSnapshot1.getValue(DatosHerra.class);
                    listCatalogo.add(c);
                }
                adapter = new AdapterPedido(HistorialActivity.this, listCatalogo);
                recyclerViewCatalogo.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}