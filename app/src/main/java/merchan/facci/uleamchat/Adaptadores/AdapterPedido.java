package merchan.facci.uleamchat.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import merchan.facci.uleamchat.Modelo.DatosHerra;
import merchan.facci.uleamchat.R;

public class AdapterPedido extends RecyclerView.Adapter<AdapterPedido.MyViewHolder>{

    private Context context;
    private List<DatosHerra> catalagos;

    public AdapterPedido(Context context, List<DatosHerra> catalagos) {
        this.context = context;
        this.catalagos = catalagos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder (LayoutInflater.from (context).inflate (R.layout.item_cardview_catalogo_pedido,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.nombre.setText (catalagos.get (position).getNombre ());
        holder.descripcion.setText (catalagos.get (position).getDescripcion ());
        holder.precio.setText (catalagos.get (position).getPrecio ());
        Picasso.get().load (catalagos.get(position).getUrlFoto ()).into (holder.urlFoto);

    }

    @Override
    public int getItemCount() {
        return catalagos.size ();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombre,descripcion, precio;
        ImageView urlFoto;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            descripcion = itemView.findViewById(R.id.catalogo_txtDescripcion);
            precio = itemView.findViewById(R.id.Catalogo_Precio);
            nombre = itemView.findViewById(R.id.catalogo_txtTitulo);
            urlFoto = itemView.findViewById(R.id.catalogo_imagen);
        }
    }
}

