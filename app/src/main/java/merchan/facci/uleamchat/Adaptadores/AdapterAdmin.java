package merchan.facci.uleamchat.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import merchan.facci.uleamchat.Modelo.DatosHerra;
import merchan.facci.uleamchat.R;

public class AdapterAdmin extends RecyclerView.Adapter<Adapter.MyViewHolder>{

    private Context context;
    private List<DatosHerra> catalagos;

    public AdapterAdmin(Context context, List<DatosHerra> catalagos) {
        this.context = context;
        this.catalagos = catalagos;
    }

    @NonNull
    @Override
    public Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Adapter.MyViewHolder(LayoutInflater.from (context).inflate (R.layout.item_cardview_catalogo,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.MyViewHolder holder, int position) {
        holder.nombre.setText (catalagos.get (position).getNombre ());
        Picasso.get().load (catalagos.get(position).getUrlFoto ()).into (holder.urlFoto);
    }

    @Override
    public int getItemCount() {
        return catalagos.size ();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        ImageView urlFoto;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre = itemView.findViewById(R.id.catalogo_txtTitulo);
            urlFoto = itemView.findViewById(R.id.catalogo_imagen);
        }
    }
}
