package merchan.facci.uleamchat.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import merchan.facci.uleamchat.HistorialActivity;
import merchan.facci.uleamchat.Modelo.DatosHerra;
import merchan.facci.uleamchat.Modelo.Usuario;
import merchan.facci.uleamchat.R;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder>{

    private Context context;
    private List<DatosHerra> catalagos;
    private List<Usuario> usuario;

    public Adapter(Context context, List<DatosHerra> catalagos) {
        this.context = context;
        this.catalagos = catalagos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder (LayoutInflater.from (context).inflate (R.layout.item_cardview_catalogo,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final String id = catalagos.get(position).getId();
        holder.nombre.setText (catalagos.get (position).getNombre ());
        Picasso.get().load (catalagos.get(position).getUrlFoto ()).into (holder.urlFoto);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HistorialActivity.class);
                intent.putExtra("hisUid",id);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return catalagos.size ();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        ImageView urlFoto;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre = itemView.findViewById(R.id.catalogo_txtTitulo);
            urlFoto = itemView.findViewById(R.id.catalogo_imagen);
        }
    }
}