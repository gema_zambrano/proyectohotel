package merchan.facci.uleamchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText Email, Contraseña;
    private Button Login, Registro;
    private ImageButton admin;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Email = findViewById(R.id.txtEmail);
        Contraseña = findViewById(R.id.txtPassword);
        Login = findViewById(R.id.btnIngreso);
        Registro = findViewById(R.id.btnRegistro);
        admin = findViewById(R.id.admin);
        mAuth = FirebaseAuth.getInstance();


        Registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, logindosActivity.class));
                finish();
            }
        });
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo = Email.getText().toString();
                String clave = Contraseña.getText().toString();

                    if (correo.equals("Admin@admin") && clave.equals("12345")){
                        Intent i= new Intent(getApplicationContext(),PrincipalAdminActivity.class);
                        startActivity(i);
                    }else if (isValidEmail(correo) && validarContraseña()) {
                        String contraseña = Contraseña.getText().toString();
                        mAuth.signInWithEmailAndPassword(correo, contraseña)
                                .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {

                                            Toast.makeText(MainActivity.this, "se ingreso correctamente", Toast.LENGTH_SHORT).show();
                                            nextActivity();


                                        } else {

                                            Toast.makeText(MainActivity.this, "ocurrio un error al inicio de sesion", Toast.LENGTH_SHORT).show();

                                        }
                                    }
                                });

                    } else{

                    Toast.makeText(MainActivity.this, "Logueado como Administrador", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private boolean isValidEmail (CharSequence target){
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean validarContraseña (){
        String contraseña;
        contraseña = Contraseña.getText().toString();
        if (contraseña.length() >=6){
            return true;
        }else return false;

    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            Toast.makeText(this,"Usuario logeado",Toast.LENGTH_SHORT).show();
            nextActivity();
        }else {
        }
    }
    private void nextActivity(){
        startActivity(new Intent(MainActivity.this, PrincipalActivity.class));
        finish();

    }

}