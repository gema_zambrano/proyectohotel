package merchan.facci.uleamchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import merchan.facci.uleamchat.Modelo.Usuario;

public class PerfilActivity extends AppCompatActivity{


    CircleImageView image_profile;
    private TextView username,correo, apellido, Direccion;
    private DatabaseReference mDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        image_profile = findViewById(R.id.profile_image);
        username = findViewById(R.id.username);
        correo = findViewById(R.id.Correo);
        apellido = findViewById(R.id.Apellido);
        Direccion = findViewById(R.id.Direccion);



    }
}
