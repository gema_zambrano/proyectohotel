package merchan.facci.uleamchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import merchan.facci.uleamchat.Adaptadores.Adapter;
import merchan.facci.uleamchat.Modelo.DatosHerra;


public class PrincipalActivity extends AppCompatActivity{

    RecyclerView recyclerViewCatalogo;
    DatabaseReference mRefCatalogo;
    ArrayList<DatosHerra> listCatalogo;
    Adapter adapter;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    ActionBar actionBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        actionBar = getSupportActionBar();
        actionBar.setTitle("Listado de Hoteles");
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);

        recyclerViewCatalogo = findViewById (R.id.recyclerview_catalogo);
        recyclerViewCatalogo.setLayoutManager (new LinearLayoutManager (this));
        listCatalogo = new ArrayList<DatosHerra> ();

        mRefCatalogo = FirebaseDatabase.getInstance ().getReference ().child ("Hoteles");

        mRefCatalogo.addValueEventListener (new ValueEventListener ( ) {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren ()){
                    DatosHerra c = dataSnapshot1.getValue (DatosHerra.class);
                    listCatalogo.add (c);
                }
                adapter = new Adapter (PrincipalActivity.this,listCatalogo);
                recyclerViewCatalogo.setAdapter (adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            firebaseAuth.signOut();
            checkUserStatus();
        }
        if (id == R.id.ic_action_carrito) {
            startActivity(new Intent(PrincipalActivity.this, HistorialActivity.class));
        }
        if (id == R.id.action_Perfil) {
            startActivity(new Intent(PrincipalActivity.this, PerfilActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;

    }
    private void checkUserStatus() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {

        } else {
            startActivity(new Intent(PrincipalActivity.this, MainActivity.class));
            finish();
        }

    }
}