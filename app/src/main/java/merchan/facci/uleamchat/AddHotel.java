package merchan.facci.uleamchat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.text.SimpleDateFormat;
import java.util.UUID;

import merchan.facci.uleamchat.Modelo.Herramientas;

public class AddHotel extends AppCompatActivity implements View.OnClickListener {

    private EditText Codigo, Nombre, Descripcion, precio;
    private Button btnEnviar, btnFoto;
    private ImageView foto;
    private Uri uri;
    private String uuid, compraFecha, userId;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, usuario;
    private StorageReference storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addhotel);

        Codigo = findViewById(R.id.txtcodigo);
        Nombre = findViewById(R.id.txtNombre);
        Descripcion = findViewById(R.id.txtDescripcion);
        precio = findViewById(R.id.txtprecio);
        btnEnviar = findViewById(R.id.btnEnviarU);
        btnFoto = findViewById(R.id.btnFoto);
        foto = findViewById(R.id.imgCargada);
        uri = null;
        uuid = UUID.randomUUID().toString();
        btnEnviar.setOnClickListener(this);
        btnFoto.setOnClickListener(this);


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Hoteles");
        storage = FirebaseStorage.getInstance().getReference();

        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {

            userId = user.getUid();
        } else {

            userId = "Anonimo";
        }



        fecha_hora();
    }

    private void fecha_hora() {
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss a");
                                String dateString = sdf.format(date);
                                compraFecha= dateString;
                            }
                        });
                    }
                } catch (InterruptedException e) {

                }
            }
        };
        t.start();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnEnviarU:
                guardarAnimal();
                break;
            case R.id.btnFoto:
                CropImage.activity()
                        .setAspectRatio(1, 1)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(this);
                break;
        }

    }

    private void guardarAnimal() {

        if (uri == null) {
            Toast.makeText(this, "DEBE ELEGIR UNA FOTO", Toast.LENGTH_SHORT).show();
            return;
        } else if (Nombre.getText().toString().isEmpty()) {
            Nombre.setError("INGRESE Un Nombre");
            Nombre.requestFocus();
        } else if (Descripcion.getText().toString().isEmpty()) {
            Descripcion.setError("INGRESE DESCRIPCION");
            Descripcion.requestFocus();
        } else if (precio.getText().toString().isEmpty()) {
            precio.setError("INGRESE UBICACION");
            precio.requestFocus();
        } else if (Codigo.getText().toString().isEmpty()) {
            Codigo.setError("INGRESE UN CODIGO");
            Codigo.requestFocus();
        } else {
            StorageReference file = storage.child("Hoteles").child(uri.getLastPathSegment());
            file.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final Uri urlFoto = taskSnapshot.getDownloadUrl();

                    Herramientas herramientas = new Herramientas();
                    herramientas.setNombre(Nombre.getText().toString());
                    herramientas.setFecha(compraFecha);
                    herramientas.setDescripcion(Descripcion.getText().toString());
                    herramientas.setCodigo(Codigo.getText().toString());
                    herramientas.setPrecio(precio.getText().toString());
                    herramientas.setUrlFoto(urlFoto.toString());

                    databaseReference.child(uuid).setValue(herramientas).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(AddHotel.this, "Habitacion Registrada", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(AddHotel.this, PrincipalUserActivity.class));
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(AddHotel.this, "Error al Registrar la Habitacion", Toast.LENGTH_SHORT).show();
                        }
                    });


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uri = result.getUri();
                foto.setImageURI(uri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, result.getError().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}